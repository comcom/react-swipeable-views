# Fork

Fork of a monorepo of `react-swipeable-views` from `tuomohopia`'s GitHub account.

Content (source in `src` & build in `lib` folder) copied from the folder:

It was necessary to make a new repo of this as neither yarn nor npm support adding monorepo git URLs.

Folder:
https://github.com/tuomohopia/react-swipeable-views/tree/animations/packages/react-swipeable-views

Github branch: `animations`
HEAD at commit: https://github.com/tuomohopia/react-swipeable-views/commit/76b983de4f6c8f59ecc9b295128432791da27d2e